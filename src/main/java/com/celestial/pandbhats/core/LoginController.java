/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.pandbhats.core;


import com.celestial.pandbhats.db.UserDAO;
import java.sql.SQLException;

/**
 *
 * @author Selvyn
 */
public class LoginController
{
    private final UserDAO itsUserdao = new UserDAO();

    public void loginUser(String who, String pwd)
    {
        try
        {
            itsUserdao.loginUser(who, pwd);
        } 
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
